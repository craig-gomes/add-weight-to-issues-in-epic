# Add weight to issues in epic

This is a simple script to add a default weight value to all issues within an epic and its epic children. If there is a weight already associated with an issue, it will not be overwritten. That can be a future feature, to force an overwrite.

Parameters:

- epic - the iid of the epic issues the weight will be added (no default value provided)
- weight - the value of the weight to add to all issues (default: 1)
- dryrun - test the run, this is a no-op so you can view the log of changes before running the script. Set to false to update the weight of all issues (default: true)

Example command line `ruby addweight.rb --epic=3928 --weight=1 --dryrun=true`
