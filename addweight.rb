require 'pp'
require 'gitlab'
require 'httparty'
require 'docopt'

def logger
    logger = Logger.new(STDOUT)
    logger.level = Logger::INFO
    logger
end

docstring = <<DOCSTRING
Add default weight 1 where weights don't contain a value for the issues within a selected epic

Usage:
#{__FILE__} --epic=<epic1> [--group=<9970>] [--weight=<1>] [--dryrun=<true|false>]
  #{__FILE__} -h | --help

Options:
  -h --help                 Show this screen.
  --epic=<epic>             List the epic for the issues you'd like to add a weight of 1 
  --group=<n>             The group where the epic resides [default: 9970]
  --weight=<n>              The value to assign to the issue weight [default: 1]
  --dryrun=<true|false>         If yes, then just data will be displayed. [default: true]

DOCSTRING

options = Docopt::docopt(docstring)

epic=options.fetch('--epic').to_i
group=options.fetch('--group').to_i
weight=options.fetch('--weight').to_i
dryrun=options.fetch('--dryrun') == 'true'
issues = []
all_epics = []

def get_epic_children(epic, group)
  logger.info("Checking for children of epic #{epic}")
  child_epics = []
  epic_list = []
  epics_url = "https://gitlab.com/api/v4/groups/#{group}/epics/#{epic}/epics?per_page=100&private_token=#{ENV['GITLAB_TOKEN']}"
  logger.debug("epics_url #{epics_url}")
  #pages = HTTParty.get(epics_url).headers['x-total-pages'].to_i
  #logger.info("Pages for epics children get #{pages}")
  #oddly at the time of writing x-total-pages wasn't in headers
  child_epics += HTTParty.get(epics_url)

  child_epics.each do |child|
    get_epic_children(child['iid'], group)
    epic_list << child['iid']
  end
  logger.info("Found #{epic_list.size} children for epic #{epic}")
  epic_list
end

logger.info("Epic #{epic} Group #{group} Weight #{weight} Dryrun #{dryrun}")
all_epics = get_epic_children(epic, group)
all_epics << epic #include the parent just in case it also has issues

all_epics.each do |epic_iid|
  url = "https://gitlab.com/api/v4/groups/#{group}/epics/#{epic_iid}/issues?per_page=100&private_token=#{ENV['GITLAB_TOKEN']}"
  logger.debug("URL: #{url}")
  pages = HTTParty.get(url).headers['x-total-pages'].to_i
  logger.info("Pages: #{pages}")
      
  pages.times do |idx|
      issues += HTTParty.get("#{url}&page=#{idx+1}")
  end

  logger.info("Epic #{epic_iid} contains #{issues.size} issues")

  issues.each do |issue|
      logger.info("Issue: #{issue['iid']} Project: #{issue['project_id']} Weight: #{issue['weight']}")
      put_url = "https://gitlab.com/api/v4/projects/#{issue['project_id']}/issues/#{issue['iid']}?weight=#{weight}&private_token=#{ENV['GITLAB_TOKEN']}"
      
      if issue['weight'] && issue['weight'].to_i > 0
        logger.info("Issue #{issue['iid']} weight is #{issue['weight']}. No updates to existing weights")
      else
        if dryrun
          logger.info("Dryrun -- Updating issue #{issue['iid']} - #{issue['title']} with weight #{weight}")
        else
          logger.info("Updating issue #{issue['iid']} with weight #{weight}")
          logger.debug("Put url #{put_url}")
          HTTParty.put(put_url)
        end

      end
  end
end